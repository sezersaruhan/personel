"use strict";


var cvApp = angular.module('personelApp', ['ui.router']);

cvApp.config(function($stateProvider, $urlRouterProvider) {
   
    $urlRouterProvider.otherwise('/home');
    
    $stateProvider
        
        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/home',
           views: {
   

    "personelInformation": {
    templateUrl: './views/personelInformation.html',
    },
    
    "works": {
    templateUrl: './views/works.html',
    },
    
    "freelance": {
    templateUrl: './views/freelance.html',
    }
  }    
        });
              
});









